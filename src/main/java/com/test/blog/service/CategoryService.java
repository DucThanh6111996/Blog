package com.test.blog.service;

import com.test.blog.entity.Category;
import com.test.blog.service.dto.CategoryDTO;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> findAllCategory();

    Optional<Category> findById(Long id);

    void create(CategoryDTO categoryDTO);

    void update(CategoryDTO categoryDTO);

    void delete(Long id);
}
