package com.test.blog.service;


import com.test.blog.entity.Author;
import com.test.blog.service.dto.AuthorDTO;

import java.util.List;
import java.util.Optional;


public interface AuthorService {

    List<Author> findAllAuthor(); // ok

    Optional<Author> findById(Long id);

    void create(AuthorDTO authorDTO);

    void update(AuthorDTO authorDTO);

    void delete(Long id);
}
