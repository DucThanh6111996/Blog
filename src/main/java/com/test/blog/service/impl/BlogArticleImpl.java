package com.test.blog.service.impl;

import com.test.blog.entity.BlogArticle;
import com.test.blog.repository.BlogArticleRepository;
import com.test.blog.service.BlogArticleService;
import com.test.blog.service.dto.BlogArticleDTO;
import ma.glasnost.orika.MapperFacade;
import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BlogArticleImpl implements BlogArticleService {

    private final BlogArticleRepository blogArticleRepository;

    private final MapperFacade mapperFacade;

    public BlogArticleImpl(BlogArticleRepository blogArticleRepository,
                           MapperFacade mapperFacade) {
        this.blogArticleRepository = blogArticleRepository;
        this.mapperFacade = mapperFacade;
    }

    @Override
    public List<BlogArticle> findAllBlogArticle() {
        return blogArticleRepository.findAll();
    }

    @Override
    public Optional<BlogArticle> findById(Long id) {
        return blogArticleRepository.findById(id);
    }

    @Override
    public void create(BlogArticleDTO blogArticleDTO) {
        BlogArticle blogArticle = mapperFacade.map(blogArticleDTO, BlogArticle.class);
        blogArticleRepository.save(blogArticle);
    }

    @Override
    public void update(BlogArticleDTO blogArticleDTO) {
        Long id = blogArticleDTO.getId();
        if (blogArticleRepository.getOne(id) == null) {
            throw new ServiceException(String.format("Blog Article with id = %d does not exist!", id));
        }
        BlogArticle blogArticle = mapperFacade.map(blogArticleDTO, BlogArticle.class);
        blogArticleRepository.save(blogArticle);
    }

    @Override
    public void delete(Long id) {
        if (blogArticleRepository.getOne(id) == null) {
            throw new ServiceException(String.format("Blog Article with id = %d does not exist!", id));
        } else {
            BlogArticle blogArticle = blogArticleRepository.getOne(id);
            blogArticle.setDeleted(true);
            blogArticleRepository.save(blogArticle);
        }
    }
}
