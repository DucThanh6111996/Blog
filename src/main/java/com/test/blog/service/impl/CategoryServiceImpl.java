package com.test.blog.service.impl;

import com.test.blog.entity.Category;
import com.test.blog.repository.CategoryRepository;
import com.test.blog.service.CategoryService;
import com.test.blog.service.dto.CategoryDTO;
import ma.glasnost.orika.MapperFacade;
import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository  categoryRepository;
    private final MapperFacade mapperFacade;

    public CategoryServiceImpl(MapperFacade mapperFacade, CategoryRepository categoryRepository){
        this.mapperFacade = mapperFacade;
        this.categoryRepository = categoryRepository;
    }
    @Override
    public List<Category> findAllCategory() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public void create(CategoryDTO categoryDTO) {
        Category category = mapperFacade.map(categoryDTO, Category.class);
        categoryRepository.save(category);
    }

    @Override
    public void update(CategoryDTO categoryDTO) {
        Long id = categoryDTO.getId();
        if (categoryRepository.getOne(id) == null) {
            throw new ServiceException(String.format("Category with id = %d does not exist!", id));
        }
        Category category = mapperFacade.map(categoryDTO, Category.class);
        categoryRepository.save(category);
    }

    @Override
    public void delete(Long id) {
        if (categoryRepository.getOne(id) == null) {
            throw new ServiceException(String.format("Author with id = %d does not exist!", id));
        } else {
            Category category = categoryRepository.getOne(id);
            category.setDeleted(true);
            categoryRepository.save(category);
        }
    }
}
