package com.test.blog.service.impl;

import com.test.blog.entity.Author;
import com.test.blog.repository.AuthorRepository;
import com.test.blog.service.AuthorService;
import com.test.blog.service.dto.AuthorDTO;
import ma.glasnost.orika.MapperFacade;

import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final MapperFacade mapperFacade;

    public AuthorServiceImpl(AuthorRepository authorRepository,
                             MapperFacade mapperFacade) {
        this.authorRepository = authorRepository;
        this.mapperFacade = mapperFacade;
    }

    @Override
    public List<Author> findAllAuthor() {
        return authorRepository.findAll();
    }

    @Override
    public Optional<Author> findById(Long id) {
        return authorRepository.findById(id);
    }

    @Override
    public void create(AuthorDTO authorDTO) {
        Author author = mapperFacade.map(authorDTO, Author.class);
        authorRepository.save(author);
    }

    @Override
    public void update(AuthorDTO authorDTO) {
        Long id = authorDTO.getId();
        if (authorRepository.getOne(id) == null) {
            throw new ServiceException(String.format("Author with id = %d does not exist!", id));
        }
        Author author = mapperFacade.map(authorDTO, Author.class);
        authorRepository.save(author);
    }

    @Override
    public void delete(Long id) {
        if (authorRepository.getOne(id) == null) {
            throw new ServiceException(String.format("Author with id = %d does not exist!", id));
        } else {
            Author author = authorRepository.getOne(id);
            author.setDeleted(true);
            authorRepository.save(author);
        }
    }

}
