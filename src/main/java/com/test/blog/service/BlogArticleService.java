package com.test.blog.service;

import com.test.blog.entity.BlogArticle;
import com.test.blog.service.dto.BlogArticleDTO;

import java.util.List;
import java.util.Optional;

public interface BlogArticleService {
    List<BlogArticle> findAllBlogArticle();

    Optional<BlogArticle> findById(Long id);

    void create(BlogArticleDTO blogArticleDTO);

    void update(BlogArticleDTO blogArticleDTO);

    void delete(Long id);
}
