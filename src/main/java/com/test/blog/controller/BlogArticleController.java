package com.test.blog.controller;

import com.test.blog.entity.BlogArticle;
import com.test.blog.service.BlogArticleService;
import com.test.blog.service.dto.BlogArticleDTO;
import com.test.blog.service.dto.GenericResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/blog-article")
public class BlogArticleController {
    private final BlogArticleService blogArticleService;

    @Autowired
    public BlogArticleController(BlogArticleService blogArticleService) {
        this.blogArticleService = blogArticleService;
    }

    @GetMapping
    public ResponseEntity getAll() {
        List<BlogArticle> blogArticles = blogArticleService.findAllBlogArticle();
        return ResponseEntity.ok(blogArticles);
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable Long id) {
        Optional<BlogArticle> blog = blogArticleService.findById(id);
        if (blog.isPresent()) {
            return ResponseEntity.ok(blog);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<GenericResponseDTO> createBlogArticle(@RequestBody BlogArticleDTO blogArticleDTO) {
        blogArticleService.create(blogArticleDTO);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(GenericResponseDTO.created());
    }

    @PutMapping
    public ResponseEntity<GenericResponseDTO> updateBlogArticle(@RequestBody BlogArticleDTO blogArticleDTO) {
        blogArticleService.update(blogArticleDTO);
        return ResponseEntity.status(HttpStatus.OK)
                .body(GenericResponseDTO.updated());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GenericResponseDTO> deleteBlogArticleById(@PathVariable("id") Long id) {
        blogArticleService.delete(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(GenericResponseDTO.deleted());
    }
}
